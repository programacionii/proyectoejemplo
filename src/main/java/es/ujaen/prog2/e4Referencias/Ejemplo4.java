/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ujaen.prog2.e4Referencias;

import java.util.ArrayList;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class Ejemplo4 {

    public void ejecutar() {

        System.out.println("");
        System.out.println("EJEMPLO 4: Referencias (Punteros)");
        System.out.println("");

        System.out.println("En java los objetos y EEDD se comportan como los punteros de C++, no existe el paso por copia en los métodos.");
        System.out.println("Esto quiere decir que si pasamos un objeto o EEDD a otro método y se modifica, el objeto cambia.");

        ArrayList<Integer> arrayInt = new ArrayList<>();
        arrayInt.add(14);
        arrayInt.add(20);
        arrayInt.add(30);

        System.out.println("Array original: " + arrayInt);

        eliminarNumeroArray(arrayInt, 0);

        System.out.println("Después de quitar el primer número: " + arrayInt);

        añadirNumeroArray(arrayInt, 40);
        añadirNumeroArray(arrayInt, 50);

        System.out.println("Después de añadir dos números: " + arrayInt);

        System.out.println("CUIDADO: si en el método se reemplaza o sustituye el objeto o EEDD se pierde la referencia al original y no se modificará.");
        
        sustituirArray(arrayInt);
        
        System.out.println("Después de sustituir el array y añadir 2 números: " + arrayInt);
        System.out.println("Como se puede ver, el array no se ha modificado, este es un error común y dificil de detectar.");

    }

    private void añadirNumeroArray(ArrayList<Integer> array, int numero) {

        array.add(numero);

    }

    private void eliminarNumeroArray(ArrayList<Integer> array, int posicion) {

        array.remove(posicion);

    }
    
    private void sustituirArray(ArrayList<Integer> array) {

        array = new ArrayList<>();
        array.add(3);
        array.add(4);

    }
}
