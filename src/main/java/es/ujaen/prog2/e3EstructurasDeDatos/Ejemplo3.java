/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ujaen.prog2.e3EstructurasDeDatos;

import java.util.ArrayList;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class Ejemplo3 {

    public void ejecutar() {
        
        System.out.println("");
        System.out.println("EJEMPLO 3: Estructuras de Datos");
        System.out.println("");

        System.out.println("En java las estructuras de datos (EEDD) difieren algo a las de C++.");
        System.out.println("Sigue existiendo el vector de tamaño fijo con llaves[], pero normalmente no se usa.");
        System.out.println("Al igual que en C++ si un vector va a tener un tamaño variable se recomendaba el uso de vector u otras EEDD predefinidas.");
        System.out.println("En Java existen los ArrayList como vectores y otras EEDD como Queue, HashMap, doubleLinkedList, etc.");
        System.out.println("El contenido de la EEDD se define como en C++ con <>.");
        System.out.println("Como norma general las EEDD no aceptan tipos simples, se debe usar su contrapartida int -> Integer.");
        System.out.println("El acceso a una posición invalida genera la excepción IndexOutOfBoundsException.");
        System.err.println("En el código de abajo hay ejemplos de uso de vectos y arrays.");

        int vInt[] = new int[3];
        int[] vInt2 = new int[3];

        vInt[1] = 20;
        int a = vInt[0];

        ArrayList<Integer> arrayInt = new ArrayList<>();

        //Añadir
        arrayInt.add(10);
        arrayInt.add(22);
        arrayInt.add(30);
        
        //Modificar
        arrayInt.set(1, 20);
        
        //Acceder
        a = arrayInt.get(a);
        
        //Eliminar
        arrayInt.remove(0);
        
        

    }

}
