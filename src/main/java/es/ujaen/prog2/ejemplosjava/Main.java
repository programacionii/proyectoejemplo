/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ujaen.prog2.ejemplosjava;

import es.ujaen.prog2.recomendaciones.Recomendaciones;
import es.ujaen.prog2.e1Tipos.Ejemplo1;
import es.ujaen.prog2.e2Clases.Ejemplo2;
import es.ujaen.prog2.e3EstructurasDeDatos.Ejemplo3;
import es.ujaen.prog2.e4Referencias.Ejemplo4;
import es.ujaen.prog2.e5MenusDeConsola.Ejemplo5;
import java.io.IOException;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class Main {

    public static void main(String[] args) throws IOException {

        Recomendaciones rec = new Recomendaciones();
        Ejemplo1 ej1 = new Ejemplo1();
        Ejemplo2 ej2 = new Ejemplo2();
        Ejemplo3 ej3 = new Ejemplo3();
        Ejemplo4 ej4 = new Ejemplo4();
        Ejemplo5 ej5 = new Ejemplo5();

        //Descomentar y comentar cada línea para ejecutar los diferentes ejemplos
        rec.ejecutar();
        ej1.ejecutar();
        ej2.ejecutar();
        ej3.ejecutar();
        ej4.ejecutar();
        ej5.ejecutar();
        
    }

}
