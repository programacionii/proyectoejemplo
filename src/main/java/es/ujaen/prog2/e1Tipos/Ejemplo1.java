/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ujaen.prog2.e1Tipos;

import java.util.ArrayList;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class Ejemplo1 {

    public void ejecutar() {
        
        System.out.println("");
        System.out.println("EJEMPLO 1: Tipos de variables");
        System.out.println("");
        
        System.out.println("En java se pueden diferenciar principalmente dos tipos de variables simples y complejas u objetos.");
        System.out.println("Las simples son los mismos que en C++ y se distinguen porque empiezan por minúscula: int, char, boolean, float...");

        int intAux = 0;
        char charAux = 'D';

        System.out.println("Las variables simples tienen su versión en objeto para poder usarlas en Estructuras de datos complejas: Integer, Character, Boolean, Float...");

        Integer integerAux = 0;
        Character characterAux = 'D';

        System.out.println("El resto de objetos son instancias de clases que vienen predefinidas en Java como String o clases que programamos nosotros mismos.");

        System.out.println("Antes de usar una variable es necesario inicializarlas, normalmente a los tipos simples se les asigna un valor,\n"
                + "y en los objetos llamamos al constructor con new. Si usamos un objeto no inicializado saltará un excepción NullPointerException");
        
        String strAux = "Hola mundo";
        String strAux2 = new String("Hola mundo");
        
        ArrayList<Integer> array = new ArrayList<>();
        
        System.out.println("Para comparar tipos complejos (String, ArrayList...) es necesario usar el método .equals(),\n"
                + "la excepción a esta regla son los tipos complejos que representan a simples, Integer, Float, etc.");

    }

}
