/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ujaen.prog2.e2Clases;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class Ejemplo2 {

    public void ejecutar() {  
        
        System.out.println("");
        System.out.println("EJEMPLO 2: Clases Java");
        System.out.println("");

        System.out.println("En java las clases son muy similares a las usadas en c++.");
        System.out.println("Para poder funcionar la clase debe estar dentro de un paquete\n"
                + "y la primera línea es el nombre del paquete, en este caso:\"package es.ujaen.prog2.E2Clases;\"");

        System.out.println("Recuerda, si necesitas asignar valores a las variables de un objeto\n"
                + "en la creación necesitas implementar un constructor. Hay un ejemplo en ClasePrueba.");

        System.out.println("Para poder crear instancias o acceder a clases que no están en el mismo paquete\n"
                + "necesitamos hacer un import, en la clase Main se pueden varios ejemplos.");
        System.out.println("Las reglas para definir si un método o variable es público o privado son iguales a C++\n"
                + "Descomenta las siguientes lineas para comprobar su comportamiento.");

        ClasePrueba prueba = new ClasePrueba(0, 1);

        int a;
        a = prueba.publico;
        a = prueba.metodoPublico();
        
//        a = prueba.privado;
//        a = prueba.metodoPrivado();

    }

}
