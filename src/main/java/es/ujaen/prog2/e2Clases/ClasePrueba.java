/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ujaen.prog2.e2Clases;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class ClasePrueba {

    public int publico;
    private int privado;

    public ClasePrueba(int publico, int privado) {
        
        this.publico = publico;
        this.privado = privado;
        
    }
    

    public int metodoPublico() {

        return publico;

    }

    private int metodoPrivado() {
        
        return privado;
        
    }

}
