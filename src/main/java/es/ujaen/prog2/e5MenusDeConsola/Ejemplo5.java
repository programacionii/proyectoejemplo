/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ujaen.prog2.e5MenusDeConsola;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class Ejemplo5 {

    public void ejecutar() throws IOException {

        System.out.println("");
        System.out.println("EJEMPLO 5: Menú de Consola");
        System.out.println("");

        System.out.println("En este ejemplo se verá un menú de consola usando métodos para dividir cada submenús dejando el código mas limpio.");

        int seleccion = -1;

        do {

            System.out.println("");
            System.out.println("Seleccione una opción:");
            System.out.println("1- Ejemplo lectura números.");
            System.out.println("2- Ejemplo lectura palabras.");
            System.out.println("0- Salir.");

            //Para leer desde la consola
            //Para que funciones es necesario poner "throws IOException" en la cabecera del método
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

            try {
                String lectura = br.readLine();
                seleccion = Integer.parseInt(lectura);
            } catch (NumberFormatException ex) {
                //Si salta la excepción es porque la lectura no es un numero entero
                seleccion = -1;
            }

            switch (seleccion) {

                case 0:
                    System.out.println("Adios.");
                    break;
                case 1:
                    leerNúmeros();
                    break;
                case 2:
                    leerPalabras();
                    break;
                default:
                    System.out.println("Opción incorrecta.");
                    break;

            }

        } while (seleccion != 0);

    }

    private void leerNúmeros() throws IOException {

        System.out.println("Menú -> Suma de números");
        System.out.println("Introduce el primer número");

        int primero = 0, segundo = 0;
        boolean lecturaCorrecta;

        do {

            lecturaCorrecta = true;
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

            try {
                String lectura = br.readLine();
                primero = Integer.parseInt(lectura);
            } catch (NumberFormatException ex) {
                //Si salta la excepción es porque la lectura no es un numero entero
                System.out.println("Error en la entrada, vuelve a probar");
                lecturaCorrecta = false;
            }
        } while (!lecturaCorrecta);

        System.out.println("Introduce el segundo número");
        do {

            lecturaCorrecta = true;
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

            try {
                String lectura = br.readLine();
                segundo = Integer.parseInt(lectura);
            } catch (NumberFormatException ex) {
                //Si salta la excepción es porque la lectura no es un numero entero
                System.out.println("Error en la entrada, vuelve a probar");
                lecturaCorrecta = false;
            }
        } while (!lecturaCorrecta);

        primero = primero + segundo;

        System.out.println("La suma de ambos número es " + primero);

    }

    private void leerPalabras() throws IOException {
        System.out.println("Menú -> Dar la vuelta a la frase.");
        System.out.println("Introduce una frase de texto.");

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        String frase = br.readLine();

        String[] fraseCortada = frase.split(" ");

        System.out.println("La frase con las palabras cambiadas de posicion: ");

        for (int i = fraseCortada.length - 1; i >= 0; i--) {

            System.out.print(fraseCortada[i] + " ");

        }

        System.out.println("");

    }

}
