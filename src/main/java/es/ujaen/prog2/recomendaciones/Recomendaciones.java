/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ujaen.prog2.recomendaciones;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class Recomendaciones {

    public void ejecutar() {

        System.out.println("");
        System.out.println("RECOMENDACIONES");
        System.out.println("");
        System.out.println("Primera:");
        System.out.println("En java se recomienda nombrar los métodos, variables, clases, etc. siguiendo un esquema CamelCase.");
        System.out.println("Empezando en minúscula y dejando la primera letra de cada en mayúscula comoPorEjemplo.");
        System.out.println("Como excepción los nombres de las clases que empiezan en mayúscula.");
        System.out.println("Durante las prácticas se admitirán otras formas de escribir el código,\n"
                + "pero que sea razonable y legible, en caso contrario podría afectar a la nota de esa práctica.");

        System.out.println("Segunda:");
        System.out.println("Los nombres de variables, clases y métodos deben de ser descriptivos, en caso contrario podría afectar a la nota de esa práctica.");

        System.out.println("Tercera:");
        System.out.println("Las estructuras condiciales y de control, como los IFs, FOR, WHILE, SWITCH, etc. tiene la misma sintaxis que en C++.");
        
    }

}
